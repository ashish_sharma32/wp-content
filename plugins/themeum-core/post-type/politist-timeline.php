<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


/**
 * Admin functions for the Event post type
 *
 * @author 		Themeum
 * @category 	Admin
 * @package 	Politist
 *-------------------------------------------------------------*/

/*--------------------------------------------------------------
*			Register Gallery Post Type
*-------------------------------------------------------------*/


function themeum_post_type_time_line()
{
	$labels = array(
		'name'                	=> _x( 'Timeline', 'Timeline', 'themeum-core' ),
		'singular_name'       	=> _x( 'Timeline', 'Timeline', 'themeum-core' ),
		'menu_name'           	=> __( 'Timeline', 'timeline' ),
		'parent_item_colon'   	=> __( 'Parent Time Line:', 'themeum-core' ),
		'all_items'           	=> __( 'All Title', 'themeum-core' ),
		'view_item'           	=> __( 'View Time Line', 'themeum-core' ),
		'add_new_item'        	=> __( 'Add New Time Line', 'themeum-core' ),
		'add_new'               => __( 'Add New', 'themeum-core' ),
		'edit_item'           	=> __( 'Edit Title', 'themeum-core' ),
		'update_item'         	=> __( 'Update Time Line', 'themeum-core' ),
		'search_items'        	=> __( 'Search Time Line', 'themeum-core' ),
		'not_found'           	=> __( 'No article found', 'themeum-core' ),
		'not_found_in_trash'  	=> __( 'No article found in Trash', 'themeum-core' )



		);

	$args = array(  
		'labels'             	=> $labels,
		'public'             	=> true,
		'publicly_queryable' 	=> true,
		'show_in_menu'       	=> true,
		'show_in_admin_bar'   	=> true,
		'can_export'          	=> true,
		'has_archive'        	=> true,
		'hierarchical'       	=> false,
		'menu_position'      	=> null,
		'menu_icon'				=> 'dashicons-welcome-write-blog',
		'supports'           	=> array( 'title'),
		);

	register_post_type('time_line', $args);

}

add_action('init','themeum_post_type_time_line');