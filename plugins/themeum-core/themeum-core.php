<?php
/*
* Plugin Name: Themeum Core
* Plugin URI: http://www.themeum.com/item/core
* Author: Themeum
* Author URI: http://www.themeum.com
* License - GNU/GPL V2 or Later
* Description: Themeum Core is a required plugin for this theme.
* Version: 1.4
*/
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// language
add_action( 'init', 'themeum_core_language_load' );
function themeum_core_language_load(){
    $plugin_dir = basename(dirname(__FILE__))."/languages/";
    load_plugin_textdomain( 'themeum-core', false, $plugin_dir );
}

if( !function_exists("themeum_cat_list") ){
    // List of Group
    function themeum_cat_list( $category ){
        global $wpdb;
        $sql = $wpdb->prepare("SELECT * FROM `".$wpdb->prefix."term_taxonomy` INNER JOIN `".$wpdb->prefix."terms` ON `".$wpdb->prefix."term_taxonomy`.`term_taxonomy_id`=`".$wpdb->prefix."terms`.`term_id` AND `".$wpdb->prefix."term_taxonomy`.`taxonomy`='%s'",$category);
        $results = $wpdb->get_results( $sql );

        $cat_list = array();
        $cat_list['All'] = 'themeumall';  
        if(is_array($results)){
            foreach ($results as $value) {
                $cat_list[$value->name] = $value->slug;
            }
        }
        return $cat_list;
    }
}

if(!function_exists('politist_excerpt_max_char')):
    function politist_excerpt_max_char($charlength) {
        $excerpt = get_the_excerpt();
        $charlength++;

        if ( mb_strlen( $excerpt ) > $charlength ) {
            $subex = mb_substr( $excerpt, 0, $charlength - 5 );
            $exwords = explode( ' ', $subex );
            $excut = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
            if ( $excut < 0 ) {
                return mb_substr( $subex, 0, $excut );
            } else {
                return $subex;
            }

        } else {
            return $excerpt;
        }
    }
endif;


// Metabox Include
include_once( 'post-type/meta_box.php' );
include_once( 'post-type/meta-box/meta-box.php' );

# photo gallery post type
include_once( 'post-type/photo-gallery.php' );
include_once( 'post-type/politist-timeline.php' );


// Redux integration
global $themeum_options; 
if ( !class_exists( 'ReduxFramework' ) ) {
    include_once( 'lib/redux/framework.php' );
    include_once( 'lib/admin-config.php' );
    include_once( 'import-functions.php' );
}
//login up
include_once( 'lib/registration.php' );

//widget
include_once( 'widget/popular-post.php' );




// New Shortcode
include_once( 'vc-addons/fontawesome-helper.php' );
include_once( 'vc-addons/currency-helper.php' );
include_once( 'vc-addons/themeum-heading.php' );
include_once( 'vc-addons/themeum-latest-news.php' );
include_once( 'vc-addons/themeum-call-to-signup.php' );
include_once( 'vc-addons/themeum-video-popup.php' );
include_once( 'vc-addons/themeum-mission-vision.php' );
include_once( 'vc-addons/themeum-donate.php' );
include_once( 'vc-addons/themeum-slider2.php' );
include_once( 'vc-addons/themeum-features.php' );
include_once( 'vc-addons/themeum-woocommerce-product.php' );
include_once( 'vc-addons/themeum-video-carousel.php' );
include_once( 'vc-addons/themeum-timeline.php' );
include_once( 'vc-addons/themeum-video-popup-timecounter.php' );
# Home 2
include_once( 'vc-addons/themeum-counter.php' );
include_once( 'vc-addons/themeum-news-feed.php' );
include_once( 'vc-addons/themeum-call-to-join.php' );
include_once( 'vc-addons/themeum-volunteer.php' );
include_once( 'vc-addons/themeum-resume.php' );

# single photo gallery
include_once( 'vc-addons/single-photo-gallery.php' );
include_once( 'vc-addons/themeum-gallery-list.php' );
include_once( 'vc-addons/politist-event-counter.php' );

add_action( 'plugins_loaded', 'load_themeum_slider' );
function load_themeum_slider(){
    include_once( 'vc-addons/themeum-slider.php' );
}


# Add CSS for Frontend
add_action( 'wp_enqueue_scripts', 'themeum_core_style' );
if(!function_exists('themeum_core_style')):
    function themeum_core_style(){

        # CSS
        wp_enqueue_style('animate',plugins_url('assets/css/animate.css',__FILE__));
        wp_enqueue_style('reduxadmincss',plugins_url('assets/css/reduxadmincss.css',__FILE__));
        wp_enqueue_style('magnific-popup',plugins_url('assets/css/magnific-popup.css',__FILE__));
        wp_enqueue_style('themeum-owl-carousel',plugins_url('assets/css/owl.carousel.css',__FILE__));
        wp_enqueue_style('themeum-core',plugins_url('assets/css/themeum-core.css',__FILE__));

        #js
        wp_enqueue_script('jquery.inview.min',plugins_url('assets/js/jquery.inview.min.js',__FILE__), array('jquery'));
        wp_enqueue_script('jquery.countdown.min',plugins_url('assets/js/jquery.countdown.min.js',__FILE__), array('jquery'));
        wp_enqueue_script('jquery.counterup.min',plugins_url('assets/js/jquery.counterup.min.js',__FILE__), array('jquery'));
        wp_enqueue_script('owl-carousel-min',plugins_url('assets/js/owl.carousel.min.js',__FILE__), array('jquery')); 
        wp_enqueue_script('jquery.magnific-popup',plugins_url('assets/js/jquery.magnific-popup.min.js',__FILE__), array('jquery'));        
        wp_enqueue_script('themeumcore-js',plugins_url('assets/js/main.js',__FILE__), array('jquery'));   
      
    }
endif;

function beackend_theme_reduxadmincss()
{
    wp_enqueue_style('reduxadmincss',plugins_url('assets/css/reduxadmincss.css',__FILE__));
}
add_action( 'admin_print_styles', 'beackend_theme_reduxadmincss' );

function beackend_theme_update_notice()
{
    wp_enqueue_style('woonotice',plugins_url('assets/css/woonotice.css',__FILE__));
}
add_action( 'admin_print_styles', 'beackend_theme_update_notice' );
