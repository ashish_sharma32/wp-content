<?php
add_shortcode( 'themeum_timeline', 'themeum_timeline_function');

function themeum_timeline_function($atts, $content = null) {
	
	$heading 	= '';
	$count_post = '';
	$class  	= '';

	extract(shortcode_atts(array(
		'heading' 		=> '',
    	'count_post' 	=>	7,		
		'class' 		=> '',
		), $atts));


	global $wpdb;
  	global $post;

  	$args = array(
      'post_type' => 'time_line',
      'order' => 'ASC',
      'posts_per_page' => esc_attr($count_post)
    );

  	$timeline = new WP_Query($args);

	$output = '';
    $output .= '<div id="resume-carousel" class="carousel slide" data-ride="carousel" data-interval="0">';
    $output .= '<div class="carousel-inner" role="listbox">';

    $i=0;
  	if ( $timeline->have_posts() ){
		while($timeline->have_posts()) {
			$timeline->the_post();
			$subtitle = get_post_meta(get_the_ID(),'themeum_timeline_subtitle',true);
			$date = get_post_meta(get_the_ID(),'themeum_timeline_date',true);
            
            if($i==0){
            $output .= '<div class="item text-center active">';
                $output .= '<h3>'.get_the_title().'</h3>';
                $output .= '<p>'.$subtitle.'</p>';
            $output .= '</div>';
            }
            else{
            $output .= '<div class="item text-center">';
                $output .= '<h3>'.get_the_title().'</h3>';
                $output .= '<p>'.$subtitle.'</p>';
            $output .= '</div>';
            }
            $i++;

		}//End of while
	}//End of IF
	$output .= '</div>';

    $output .= '<ol class="carousel-indicators">';
    $i=0;
    if ( $timeline->have_posts() ){
        while($timeline->have_posts()) {
            $timeline->the_post();
            $date = get_post_meta(get_the_ID(),'themeum_timeline_date',true);
            
            $output .= '<li data-target="#resume-carousel" data-slide-to="'.$i++.'"><span>'.$date.'</span></li>';

        }//End of while
    }//End of IF
    $output .= '</ol>';

    $output .= '</div>';      

	return $output;
}


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("Timeline", 'themeum-core'),
		"base" => "themeum_timeline",
		'icon' => 'icon-thm-latest-news',
		"class" => "",
		"description" => esc_html__("Timeline", 'themeum-core'),
		"category" => esc_html__('Politist', 'themeum-core'),
		"params" => array(

			array(
				"type" => "textfield",
				"heading" => esc_html__("Heading", 'themeum-core'),
				"param_name" => "heading",
				"value" => "",
				),		

			array(
				"type" => "textfield",
				"heading" => esc_html__("Custom Class", 'themeum-core'),
				"param_name" => "class",
				"value" => "",
				),

			)

		));
}