<?php
if ( ! function_exists( 'getCurrencyList' ) ) {

	function getCurrencyList(){
		return array(
		'Select one' 			  => '',			
		'United States dollar($)' => 'USD:$',
		'British pound(£)'		  => 'GBP:£',
		'Russian Ruble(₽)' 		  => 'RUB:₽',
		'Brazilian Real(R$)'	  => 'BRL:R$',
		'Canadian Dollar($)'	  => 'CAD:$',
		'Czech Koruna(Kč)'		  => 'CZK:Kč',
		'Danish Krone(kr.)'		  => 'DKK:kr.',
		'Euro(€)'				  => 'EUR:€',
		'Hong Kong Dollar(HK$)'	  => 'HKD:HK$',
		'Hungarian Forint(Ft)'	  => 'HUF:Ft',
		'Israeli New Sheqel(₪)'	  => 'ILS:₪',
		'Japanese Yen(¥)'		  => 'JPY:¥',
		'Malaysian Ringgit(RM)'	  => 'MYR:RM',
		'Mexican Peso(Mex$)'	  => 'MXN:Mex$',
		'Norwegian Krone(kr)'	  => 'NOK:kr',
		'New Zealand Dollar($)'	  => 'NZD:$',
		'Philippine Peso(₱)'	  => 'PHP:₱',
		'Polish Zloty(zł)'		  => 'PLN:zł',
		'Singapore Dollar($)'	  => 'SGD:$',
		'Swedish Krona(kr)'		  => 'SEK:kr',
		'Swiss Franc(CHF)'		  => 'CHF:CHF',
		'Taiwan New Dollar(角)'	  => 'TWD:角',
		'Thai Baht(฿)'			  => 'THB:฿',
		'Turkish Lira(TRY)'		  => 'TRY:TRY'
		);
	}
}