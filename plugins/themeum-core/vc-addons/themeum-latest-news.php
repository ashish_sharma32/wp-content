<?php
add_shortcode( 'themeum_latest_news', 'themeum_latest_news_function');

function themeum_latest_news_function($atts, $content = null) {
	
	$category 	= 'themeumall';
	$number 	= '5';
	$class  	= '';
	$order_by	= 'date';
	$order		= 'DESC';

	extract(shortcode_atts(array(
		'category' 		=> 'themeumall',
		'number' 		=> '5',
		'class' 		=> '',
		'order_by'		=> 'date',
		'order'			=> 'DESC',
		), $atts));

 	global $post;
	global $wpdb;
	$output = '';
	
	// Basic Query
    $args = array(
                'post_status'		=> 'publish',
				'posts_per_page'	=> esc_attr($number),
				'order'				=> $order,
				'orderby'			=> $order_by
			);

    // Category Add
	if( ( $category != '' ) && ( $category != 'themeumall' ) ){
		$args2 = array( 'tax_query' => array(
											array(
												'taxonomy' => 'category',
												'field'    => 'slug',
												'terms'    => $category,
											),
										),
						);
		$args = array_merge( $args,$args2 );
	}
	$data = new WP_Query($args);

//$output .= print_r($data,true);
	
			
        $output .= '<div class="latest-news">';
                $output .= '<div class="row">';
                    $i=1;
                    // The Loop
					if ( $data->have_posts() ) {
						while ( $data->have_posts() ) {
							$data->the_post();

						if($i==1){
		                    $output .= '<div class="col-md-6 col-xs-12">';
		                        $output .= '<div class="news-wrap">';
		                            $output .= '<div class="news-img">';
		                                $output .= '<div class="news-date">';
		                                    $output .= '<p>'. get_the_date( get_option('date_format')) .'</p>';
		                                $output .= '</div>';
		                                $output .= '<a href="'.get_the_permalink().'">'.get_the_post_thumbnail(get_the_ID(),'politist-medium', array('class' => 'img-responsive')).'</a>';
		                            $output .= '</div>';
		                            $output .= '<h3><a href="'.get_the_permalink().'">'. get_the_title() .'</a></h3>';
		                            $output .= '<div class="short-desc">'. politist_excerpt_max_char(200). '</div>';
		                        $output .= '</div>';
		                    $output .= '</div>';
	                    }

	                    if($i==1 && ($data->found_posts > 1) ){ $output .= '<div class="col-md-6 col-xs-12">'; }
	                        
	                        if( $i!=1 ){
		                        $output .= '<div class="thumbnail-news">';
		                            $output .= '<div class="news-img pull-left">';
		                                $output .= '<div class="news-date">';
		                                    $output .= '<p>'. get_the_date( get_option('date_format')) .'</p>';
		                                $output .= '</div>';
		                                $output .= '<a href="'.get_the_permalink().'">'.get_the_post_thumbnail(get_the_ID(),'politist-small', array('class' => 'img-responsive')).'</a>';
		                            $output .= '</div>';
		                            $output .= '<div class="small-news pull-left">';
		                                $output .= '<h4><a href="'.get_the_permalink().'">'. get_the_title() .'</a></h4>';
		                                $output .= '<div class="short-desc">'.politist_excerpt_max_char(100).'</div>';
		                            $output .= '</div>';
		                            $output .= '<div class="clearfix"></div>';
		                        $output .= '</div>';
	                    	}
	                    $i++;
                		}
                	}
					wp_reset_postdata();
					if(($data->found_posts > 1)){ $output .= '</div>'; }
               $output .= '</div>';
        $output .= '</div>';



	return $output;
}


//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
	vc_map(array(
		"name" => esc_html__("Latest News", 'themeum-core'),
		"base" => "themeum_latest_news",
		'icon' => 'icon-thm-latest-news',
		"class" => "",
		"description" => esc_html__("Latest News", 'themeum-core'),
		"category" => esc_html__('Politist', 'themeum-core'),
		"params" => array(				


			array(
				"type" => "dropdown",
				"heading" => esc_html__("Category Filter", 'themeum-core'),
				"param_name" => "category",
				"value" => themeum_cat_list('category'),
				),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Number of items", 'themeum-core'),
				"param_name" => "number",
				"value" => "5",
				),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("OderBy", 'themeum-core'),
				"param_name" => "order_by",
				"value" => array('Select'=>'','Date'=>'date','Title'=>'title','Modified'=>'modified','Author'=>'author','Random'=>'rand'),
				),

			array(
				"type" => "dropdown",
				"heading" => esc_html__("Order", 'themeum-core'),
				"param_name" => "order",
				"value" => array('Select'=>'','DESC'=>'DESC','ASC'=>'ASC'),
				),

			array(
				"type" => "textfield",
				"heading" => esc_html__("Custom Class", 'themeum-core'),
				"param_name" => "class",
				"value" => "",
				),

			)

		));
}