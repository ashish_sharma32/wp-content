<?php
add_shortcode( 'themeum_donate', 'themeum_donate_function');


function themeum_donate_function($atts, $content = null) {

	$currency 	= '';
	$paypalid 	= '';	
	$class		= '';
	$target 	= '';
	$more 		= '';
	$amount1	= '';
	$amount2	= '';
	$amount3	= '';		
	$amount4	= '';
	$btn_name	= 'Donate Now';

	extract(shortcode_atts(array(
		'currency' 	=> '',
		'paypalid'	=> '',
		'target' 	=> '_self',
		'more' 		=> '',
		'amount1'	=> '',
		'amount2'	=> '',
		'amount3'	=> '',
		'amount4'	=> '',	
		'btn_name'	=> 'Donate Now',	
		'class'		=> '',								
		), $atts));


 	$donations = array(intval($amount1), intval($amount2), intval($amount3), intval($amount4));
	$output  = '<div class="politist-addon-donation ' . $class . '">';
	$output .= '<div class="donation-ammount-wrap donate-buttons" data-currency="'.$currency.'" data-pid="'.$paypalid.'">';

	$crcy_code = explode(':', $currency);

		foreach ($donations as $key => $donation) {
			$active = ( $key > 1 && ((count($donations)/$key) == $key) ) ? 'active' : '' ;
			if($key==2){
				$output .= '<input class="donation-input '.$active.'" type="text" name="amount" value="'. $crcy_code[1] . $donation .'" readonly>';	
			}else{
				$output .= '<input class="donation-input" type="text" name="amount" value="'. $crcy_code[1] . $donation .'" readonly>';					
			}	
		}
		if ($more) {
			$output .= '<input class="donation-input input-text" type="number" name="amount" autocomplete="off" placeholder="'.esc_html__( 'More', 'politist' ).'" min="1">';
		}
		
	$output .= '</div>'; //.donation-ammount

	$output .= '<div class="donation-button">';
		$output .= '<a href="#" target="' . $target . '" class="filled-button donation-button-link">' . $btn_name . '</a>';
	$output .= '</div>'; //.donation-button
	
	$output .= '</div>'; //.politist-addon-donation

	return $output;
}

//Visual Composer
if (class_exists('WPBakeryVisualComposerAbstract')) {
vc_map(array(
	"name" 		=> __("Donate Now", "themeum"),
	"base" 		=> "themeum_donate",
	'icon' 		=> 'icon-thm-title',
	"class" 	=> "",
	"description" => __("Donate now widget", "themeum"),
	"category" 	=> __('Politist', "themeum"),
	"params" 	=> array(			

		array(
			"type" => "dropdown",
			"heading" => __("Select Currency", "themeum"),
			"param_name" => "currency",
			"value" => getCurrencyList(),
		),

		array(
			"type" => "textfield",
			"heading" => __("Paypal ID", "themeum"),
			"param_name" => "paypalid",
			"value" => "",
		),

		array(
			"type" => "dropdown",
			"heading" => __("The target attribute for link", "themeum"),
			"param_name" => "target",
			"value" => array('Select'=>'','Same Window'=>'_self','New Window'=>'_blank'),
		),

		array(
			"type" => "dropdown",
			"heading" => __("More", "themeum"),
			"param_name" => "more",
			"value" => array('Select'=>'','Yes' => 1,'No' => 0),
		),

		// Amount-1 Number
		array(
			"type" => "textfield",
			"heading" => __("Amount-1: ", "themeum"),
			"param_name" => "amount1",
			"value" => "",
		),		

		// Amount-2 Number
		array(
			"type" => "textfield",
			"heading" => __("Amount-2: ", "themeum"),
			"param_name" => "amount2",
			"value" => "",
		),

		// Amount-3 Number
		array(
			"type" => "textfield",
			"heading" => __("Amount-3: ", "themeum"),
			"param_name" => "amount3",
			"value" => "",
		),		

		// Amount-4 Number
		array(
			"type" => "textfield",
			"heading" => __("Amount-4: ", "themeum"),
			"param_name" => "amount4",
			"value" => "",
		),

		array(
			"type" => "textfield",
			"heading" => __("Button name: Ex. Donate Now ", "themeum"),
			"param_name" => "btn_name",
			"value" => "Donate Now",
		),		

		array(
			"type" => "textfield",
			"heading" => __("Custom CSS", "themeum"),
			"param_name" => "class",
			"value" => "",
			),		

		)
	));
}
?>