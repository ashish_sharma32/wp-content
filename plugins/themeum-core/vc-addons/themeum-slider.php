<?php
if(!class_exists('ThemeumSliderShortcode'))
{
	class ThemeumSliderShortcode
	{
		
		function __construct()
		{	
			$active_counter = 0;
			add_action('init', array($this, 'add_themeum_slider'));			
			add_shortcode( 'themeum_slider_wrap', array($this, 'themeum_slider_wrap' ) );
			add_shortcode( 'themeum_slider_item', array($this, 'themeum_slider_item' ) );
		}

		function themeum_slider_wrap($atts, $content = null)
		{
			$class 				= '';
			$time 				= '';
			$disable_slider 	= '';
			extract(shortcode_atts(array(
				'class' 			=> '',
				'time' 				=> '',
				'disable_slider' 	=> '',
			), $atts));
			if($disable_slider == 'enable'){
		        $time = 'false';
		    }
			$randId = rand(10,100);
			$output  = '<div id="home-one-crousel'.$randId.'" class="home-one-crousel carousel slide ' . esc_attr($class) . '">';
				$output .= '<div class="carousel-inner">';
							$output .= do_shortcode($content);
							$output  .= '<div class="slider-overlay"></div>';
				$output .= '</div>';//carousel-inner
                // Controls
                $output .= '<a class="left carousel-control" href="#home-one-crousel'.$randId.'" role="button" data-slide="prev">';
                    $output .= '<i class="fa fa-angle-left"></i>';
                $output .= '</a>';
                $output .= '<a class="right carousel-control" href="#home-one-crousel'.$randId.'" role="button" data-slide="next">';
                    $output .= '<i class="fa fa-angle-right"></i>';
                $output .= '</a>';
			$output .= '</div>';//#home-one-crousel

			// JS time
    		$output .= "<script type='text/javascript'>jQuery(document).ready(function() { jQuery('#home-one-crousel".$randId."').carousel({ interval: ".$time." }) });</script>";
			return $output;
		}

		function themeum_slider_item($atts,$content = null)
		{
			$slider_type = '';
			$title = '';
			$subtitle = '';
			$image = '';
			$btn_url = '';
			$btn_text = '';
			extract(shortcode_atts(array(
				'slider_type'   => 'text_slide',
				'title'     	=> '',
				'subtitle'  	=> '',
				'btn_url' 		=> '',
				'btn_text' 		=> '',
				'image' 		=> '',
			), $atts));
			
			$style = '';
			$output = '';
			$src_image   = wp_get_attachment_image_src($image, 'full');
			if ($slider_type == 'image_slide') {
				$style = 'style="background: #333 url('.esc_url($src_image[0]).'); background-size: cover; background-repeat: no-repeat;background-position: center center;"';
			} else {
				$style = 'style="overflow: hidden;backface-visibility: hidden;"';
			}	
			
			//foreach ($title as $value) {
				global $active_counter;
				if( $active_counter == 0 ){
	            $output   .= '<div class="item active" '.$style.'>';
	            $active_counter = 2;
	            }else{
	            $output   .= '<div class="item" '.$style.'>';
	            }
					$output  .= '<div class="container">';
						$output  .= '<div class="slider-content text-left">';
								if ($title) {
									$output  .= '<h2 class="first-animation">'. esc_attr($title) .'</h2>';
								}
								if ($subtitle) {
									$output  .= '<h3 class="second-animation">'. $subtitle .'</h3>';
								}
								if ($btn_url) {
									$output  .= '<a class="bordered-button fourth-animation" href="'.esc_url($btn_url).'" target="_blank">'.esc_attr($btn_text).'<i class="fa fa-long-arrow-right"></i></a>';
								}
						$output  .= '</div>';//slider-content
						if ($slider_type == 'image_slide') {
							$output  .= '<div class="slider-overlay"></div>';
						}
					$output  .= '</div>';//container
				$output  .= '</div>';//item
				
			return $output;
		}
		
		// Shortcode Functions for frontend editor
		function front_themeum_slider_wrap($atts, $content = null)
		{
			// Do nothing
			$class 				= '';
			$time 				= '';
			$disable_slider 	= '';
			extract(shortcode_atts(array(
				'class' 			=> '',
				'time' 				=> '',
				'disable_slider' 	=> '',
			), $atts));

			    if($disable_slider == 'enable'){
			        $time = 'false';
			    }	
				$output  = '<div id="home-two-crousel" class="carousel carousel-fade slide ' . esc_attr($class) . '">';
					$output .= '<div class="carousel-inner">';
								$output .= do_shortcode($content);
					$output .= '</div>';//carousel-inner

	                // Controls
	                $output .= '<a class="left carousel-control" href="#home-two-crousel" role="button" data-slide="prev">';
	                    $output .= '<i class="fa fa-angle-left"></i>';
	                $output .= '</a>';
	                $output .= '<a class="right carousel-control" href="#home-two-crousel" role="button" data-slide="next">';
	                    $output .= '<i class="fa fa-angle-right"></i>';
	                $output .= '</a>';
				$output .= '</div>';//#home-two-crousel

				// JS time
    			$output .= "<script type='text/javascript'>jQuery(document).ready(function() { jQuery('#home-one-crousel".$randId."').carousel({ interval: ".$time." }) });</script>";

			return $output;
		}
		function front_themeum_slider_item($atts,$content = null)
		{
			// Do nothing
			$title = '';
			$subtitle = '';
			$image = '';
			$btn_url = '';
			$btn_text = '';
			extract(shortcode_atts(array(
				'title'     => '',
				'subtitle'  => '',
				'btn_url' 	=> '',
				'btn_text' 	=> '',
				'image' 	=> '',
			), $atts));
			
			$style = '';
			$src_image   = wp_get_attachment_image_src($image, 'full');
			$style = 'style="background: #333 url('.esc_url($src_image[0]).') no-repeat center center cover; overflow: hidden;backface-visibility: hidden;padding: 154px 0 145px;"';

			$output = '<li class="icon_list_item">';
			$output   = '<div class="item" '.$style.'>';
				$output  .= '<div class="slider-content">';
						if ($title) {
							$output  .= '<h2  data-animation="animated zoomInLeft">'. esc_attr($title) .'</h2>';
						}
						if ($subtitle) {
							$output  .= '<h3 data-animation="animated zoomInRight">'. esc_attr($subtitle) .'</h3>';
						}
						if ($btn_url) {
							$output  .= '<a href="'.esc_url($btn_url).'" class="bordered-button" data-animation="animated zoomInUp">'.esc_attr($btn_text).'<i class="fa fa-long-arrow-right"></i></a>';
						}
				$output  .= '</div>';//slider-content
				$output .= wpb_js_remove_wpautop($content, true);
			$output  .= '</div>';//item

			return $output;
		}
		function add_themeum_slider()
		{
			if(function_exists('vc_map'))
			{
				vc_map(
				array(
				   "name" => __("Themeum Slider","themeum-core"),
				   "base" => "themeum_slider_wrap",
				   "class" => "",
				   "icon" => "icon-slider-wrap",
				   "category" => "Politist",
				   "as_parent" => array('only' => 'themeum_slider_item'),
				   "description" => __("Text blocks connected together in one list.","themeum-core"),
				   "content_element" => true,
				   "show_settings_on_create" => true,
				   "params" => array(
						array(
							"type" => "textfield",
							"class" => "",
							"heading" => __("Add Custom Class","themeum-core"),
							"param_name" => "class",
							"description" => __("Add Custom Class","themeum-core")
						),	

			            array(
			                "type" => "checkbox",
			                "class" => "",
			                "heading" => esc_html__("Disable Auto Slide: ","themeum-core"),
			                "param_name" => "disable_slider",
			                "value" => array ( esc_html__('Disable','themeum-core') => 'enable'),
			                "description" => esc_html__("If you want disable slide check this.","themeum-core"),
			            ),

			            array(
			                "type" => "textfield",
			                "heading" => esc_html__("Sliding Time(Milliseconds Ex: 4000)", "themeum-core"),
			                "param_name" => "time",
			                "value" => "3000",
			                ),

					),
					"js_view" => 'VcColumnView'
				));
				// Add slider Item
				vc_map(
					array(
					   "name" => __("Themeum Slider Item","themeum-core"),
					   "base" => "themeum_slider_item",
					   "class" => "",
					   "icon" => "icon-slider-list",
					   "category" => "Politist",
					   "content_element" => true,
					   "as_child" => array('only' => 'themeum_slider_wrap'),
					   "is_container"    => false,
					   "params" => array(
					   		array(
								"type" => "dropdown",
								"heading" => esc_html__("Slider Type", 'themeum-core'),
								"param_name" => "slider_type",
								"value" => array('Select'=>'','Only Text Slide'=>'text_slide','Slide With Image '=>'image_slide'),
							),	
							array(
								"type" => "textfield",
								"class" => "",
								"heading" => __("Title","themeum-core"),
								"param_name" => "title",
								"description" => __("Title","themeum-core")
							),
							array(
								"type" => "textarea",
								"class" => "",
								"heading" => __("Sub Title","themeum-core"),
								"param_name" => "subtitle",
								"description" => __("Sub Title","themeum-core")
							),	
							array(
								"type" => "textfield",
								"class" => "",
								"heading" => __("Add Button URL","themeum-core"),
								"param_name" => "btn_url",
								"description" => __("Add URL","themeum-core")
							),							
							array(
								"type" => "textfield",
								"class" => "",
								"heading" => __("Add Button Text","themeum-core"),
								"param_name" => "btn_text",
							),														
							array(
								"type" => "attach_image",
								"heading" => esc_html__("Upload Slider Image", 'themeum-core'),
								"param_name" => "image",
								"value" => "",
							),	
					   )
					) 
				);
			}//endif
		}
	}
}
global $ThemeumSliderShortcode;
if(class_exists('WPBakeryShortCodesContainer'))
{
	class WPBakeryShortCode_themeum_slider_wrap extends WPBakeryShortCodesContainer {
        function content( $atts, $content = null ) {
            global $ThemeumSliderShortcode;
            return $ThemeumSliderShortcode->front_themeum_slider_wrap($atts, $content);
        }
	}
	class WPBakeryShortCode_themeum_slider_item extends WPBakeryShortCode {
        function content($atts, $content = null ) {
            global $ThemeumSliderShortcode;
            return $ThemeumSliderShortcode->front_themeum_slider_item($atts, $content);
        }
	}
}
if(class_exists('ThemeumSliderShortcode'))
{
	$ThemeumSliderShortcode = new ThemeumSliderShortcode;
}