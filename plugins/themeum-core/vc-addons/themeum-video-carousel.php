<?php
if(!class_exists('ThemeumVideoCarousel'))
{
	class ThemeumVideoCarousel
	{
		
		function __construct()
		{	
			add_action('init', array($this, 'add_themeum_video_carousel'));			
			add_shortcode( 'themeum_video_carousel_wrap', array($this, 'themeum_video_carousel_wrap' ) );
			add_shortcode( 'themeum_video_carousel_item', array($this, 'themeum_video_carousel_item' ) );
		}

		function themeum_video_carousel_wrap($atts, $content = null)
		{
			$class 				= '';
			extract(shortcode_atts(array(
				'class' 			=> '',
			), $atts));
			$output = '<div id="themeum-video-carosuel" class="adons-themeum-video-carosuel '.esc_attr($class).'">';
				$output  .= '<div class="themeum-video-carosuel owl-carousel owl-theme">';
						$output .= do_shortcode($content);
				$output .= '</div>';//.themeum-video-carosuel

				$output .= '<div class="owl-controls controller">';
				$output .= '<a class="owl-control videoPrev"><span><i class="left fa fa-long-arrow-left"></i></span></a>';
				$output .= '<a class="owl-control videoNext"><span><i class="right fa fa-long-arrow-right"></i></span></a>';
				$output .= '</div>'; //.controls

			$output .= '</div>'; //.addon-themeum-video-carosuel
			return $output;
		}

		function themeum_video_carousel_item($atts,$content = null)
		{
			$image 				= '';
			$video_id 			= '';
			$video_type 		= 'youtube';

			extract(shortcode_atts(array(
				'image' 			=> '',
				'video_id' 			=> '',
				'video_type' 		=> 'youtube',

			), $atts));
			
			$output = '';
			$src_image   = wp_get_attachment_image_src($image, 'full');
	
            $output   .= '<div class="item text-center">';
            $output   .= '<div class="champ-video">';
				if ($src_image) {
                    $output  .= '<img src="'.esc_url($src_image[0]).'" alt="">';
                }
              	if($video_type == 'youtube'){
              		$output   .= '<a href="https://www.youtube.com/watch?v='.$video_id.'"><i class="fa fa-youtube-play"></i></a>';                
              	} elseif ($video_type == 'vimeo') {
                   $output   .= '<a href="https://vimeo.com/32077374/'.$video_id.'"><i class="fa fa-youtube-play"></i></a>';
                } else{
                  $output   .= '<a href="https://www.youtube.com/watch?v='.$video_id.'"><i class="fa fa-youtube-play"></i></a>';    
               	}

			$output  .= '</div>';//champ-video
			$output  .= '</div>';//text-center
				
			return $output;
		}
		
		// Shortcode Functions for frontend editor
		function front_themeum_video_carousel_wrap($atts, $content = null)
		{
			// Do nothing
				$class 				= '';
			extract(shortcode_atts(array(
				'class' 			=> '',
			), $atts));

			$output = '<div id="themeum-video-carosuel" class="adons-themeum-video-carosuel'.esc_attr($class).'">';
				$output  .= '<div class="themeum-video-carosuel owl-carousel owl-theme">';
						$output .= do_shortcode($content);
				$output .= '</div>';//.themeum-video-carosuel
			$output .= '</div>'; //.addon-themeum-video-carosuel


			return $output;
		}
		function front_themeum_video_carousel_item($atts,$content = null)
		{
			// Do nothing
			$image 				= '';
			$video_id 			= '';
			$video_type 		= 'youtube';
			extract(shortcode_atts(array(
				'image' 			=> '',
				'video_id' 			=> '',
				'video_type' 		=> 'youtube',
			), $atts));
			
			$src_image   = wp_get_attachment_image_src($image, 'full');


	        $output   = '<div class="item text-center">';
	            $output   .= '<div class="champ-video">';
					if ($src_image) {
                        $output  .= '<img src="'.esc_url($src_image[0]).'" alt="">';
                    }
	              	if($video_type == 'youtube'){
	              		$output   .= '<a href="https://www.youtube.com/watch?v='.$video_id.'"><i class="fa fa-youtube-play"></i></a>';                
	              	} elseif ($video_type == 'vimeo') {
	                   $output   .= '<a href="https://vimeo.com/32077374/'.$video_id.'"><i class="fa fa-youtube-play"></i></a>';
	                } else {
	                  $output   .= '<a href="https://www.youtube.com/watch?v='.$video_id.'"><i class="fa fa-youtube-play"></i></a>';    
	               	}  
				$output .= wpb_js_remove_wpautop($content, true);
			$output  .= '</div>';//champ-video
			$output  .= '</div>';//item

			return $output;
		}
		function add_themeum_video_carousel()
		{
			if(function_exists('vc_map'))
			{
				vc_map(
				array(
				   "name" => __("Themeum Video Carousel","themeum-core"),
				   "base" => "themeum_video_carousel_wrap",
				   "class" => "",
				   "icon" => "icon-slider-wrap",
				   "category" => "Politist",
				   "as_parent" => array('only' => 'themeum_video_carousel_item'),
				   "description" => __("Text blocks connected together in one list.","themeum-core"),
				   "content_element" => true,
				   "show_settings_on_create" => true,
				   "params" => array(

						array(
							"type" => "textfield",
							"class" => "",
							"heading" => __("Add Custom Class","themeum-core"),
							"param_name" => "class",
							"description" => __("Add Custom Class","themeum-core")
						),	

					),
					"js_view" => 'VcColumnView'
				));
				// Add slider Item
				vc_map(
					array(
					   "name" => __("Video Carousel Item","themeum-core"),
					   "base" => "themeum_video_carousel_item",
					   "class" => "",
					   "icon" => "icon-slider-list",
					   "category" => "Politist",
					   "content_element" => true,
					   "as_child" => array('only' => 'themeum_video_carousel_wrap'),
					   "is_container"    => false,
					   "params" => array(
	
					   		array(
								"type" => "dropdown",
								"heading" => esc_html__("Video Type", 'themeum-core'),
								"param_name" => "video_type",
								"value" => array('Select'=>'','Youtube'=>'youtube','Vimeo'=>'vimeo'),
							),	
							array(
								"type" => "textfield",
								"class" => "",
								"heading" => __("Video ID","themeum-core"),
								"param_name" => "video_id",
								"description" => __("Add Video ID Ex. ltTMEm09ljA ","themeum-core")
							),						
							array(
								"type" => "attach_image",
								"heading" => esc_html__("Upload Slider Image", 'themeum-core'),
								"param_name" => "image",
								"value" => "",
							),		
					   )
					) 
				);
			}//endif
		}
	}
}
global $ThemeumVideoCarousel;
if(class_exists('WPBakeryShortCodesContainer'))
{
	class WPBakeryShortCode_themeum_video_carousel_wrap extends WPBakeryShortCodesContainer {
        function content( $atts, $content = null ) {
            global $ThemeumVideoCarousel;
            return $ThemeumVideoCarousel->front_themeum_video_carousel_wrap($atts, $content);
        }
	}
	class WPBakeryShortCode_themeum_video_carousel_item extends WPBakeryShortCode {
        function content($atts, $content = null ) {
            global $ThemeumVideoCarousel;
            return $ThemeumVideoCarousel->front_themeum_video_carousel_item($atts, $content);
        }
	}
}
if(class_exists('ThemeumVideoCarousel'))
{
	$ThemeumVideoCarousel = new ThemeumVideoCarousel;
}